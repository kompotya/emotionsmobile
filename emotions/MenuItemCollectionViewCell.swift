//
//  MenuItemCollectionViewCell.swift
//  emotions
//
//  Created by Michael Ovsienko on 13.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class MenuItemCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var menuItemImage: UIImageView!
    @IBOutlet weak var menuItemLabel: UILabel!
}
