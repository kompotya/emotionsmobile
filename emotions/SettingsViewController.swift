//
//  SettingsViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 13.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    
    @IBOutlet weak var showSpeedTextField: UITextField!
    @IBOutlet weak var speedSlider: UISlider!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        showSpeedTextField.text = "\(Int(speedSlider.value))"
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    @IBAction func onSliderValueChanged(_ sender: Any) {
        self.showSpeedTextField.text = "\(Int(self.speedSlider.value))"
    }
}
