//
//  TheoryListViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 13.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class TheoryListViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    let emotionNames : [String] = ["Печаль","Гнев","Страх","Удивление","Радость","Отвращение","Презрение"]
    @IBOutlet weak var collectionViewTheoryItems: UICollectionView!
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return emotionNames.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell (withReuseIdentifier: "cell_item_theory", for: indexPath as IndexPath) as! TheoryItemCollectionViewCell
        cell.theoryNameButton.setTitle(emotionNames[indexPath.row], for: .normal)
        let colors :[UIColor] = [UIColor.yellow, UIColor.gray]
        cell.theoryNameButton.layer.borderWidth = 1.0
        cell.theoryNameButton.layer.borderColor = UIColor.black.cgColor
        cell.theoryNameButton.tag = indexPath.row
        cell.theoryNameButton.addTarget(self, action: #selector(self.showDetails), for: .touchUpInside)
//        cell.theoryNameButton.applyGradient(withColours: colors, gradientOrientation: .topLeftBottomRight)
        return cell
    }
    
    func showDetails (sender : UIButton){
        let index = sender.tag
        if index == 0 {
                      // It is instance of  `NewViewController` from storyboard
            var vc : EmotionViewController = self.storyboard!.instantiateViewController(withIdentifier: "EmotionViewController") as! EmotionViewController

            
            
            vc.emotionDescriptionLabel = "description"
            vc.navigationTitle = emotionNames[index]
            
            let image = UIImage(named: "logoEmotions")!
            vc.emotionImage = image
            
  

            self.navigationController?.pushViewController(vc, animated: true)
        }

    }
    
    
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
typealias GradientPoints = (startPoint: CGPoint, endPoint: CGPoint)

enum GradientOrientation {
    case topRightBottomLeft
    case topLeftBottomRight
    case horizontal
    case vertical
    
    var startPoint : CGPoint {
        get { return points.startPoint }
    }
    
    var endPoint : CGPoint {
        get { return points.endPoint }
    }
    
    var points : GradientPoints {
        get {
            switch(self) {
            case .topRightBottomLeft:
                return (CGPoint.init(x: 0.0,y: 1.0), CGPoint.init(x: 1.0,y: 0.0))
            case .topLeftBottomRight:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 1,y: 1))
            case .horizontal:
                return (CGPoint.init(x: 0.0,y: 0.5), CGPoint.init(x: 1.0,y: 0.5))
            case .vertical:
                return (CGPoint.init(x: 0.0,y: 0.0), CGPoint.init(x: 0.0,y: 1.0))
            }
        }
    }
}
extension UIView {
    
    func applyGradient(withColours colours: [UIColor], locations: [NSNumber]? = nil) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
    }
    
    func applyGradient(withColours colours: [UIColor], gradientOrientation orientation: GradientOrientation) -> Void {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.startPoint = orientation.startPoint
        gradient.endPoint = orientation.endPoint
        self.layer.insertSublayer(gradient, at: 0)
    }
}
