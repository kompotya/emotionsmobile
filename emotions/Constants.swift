//
//  Constants.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
import UIKit

class Constants {
    public static let CORNER_RADIUS : CGFloat = 5;
}
