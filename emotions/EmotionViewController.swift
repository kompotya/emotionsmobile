//
//  EmotionViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 13.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class EmotionViewController: UIViewController {


    @IBOutlet weak var imageEmotion: UIImageView!
    @IBOutlet weak var emotionDescription: UITextView!
    
    
    var navigationTitle : String = ""
    var emotionImage : UIImage = UIImage(named: "logoEmotions")!
    var emotionDescriptionLabel : String = ""
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationItem.title = navigationTitle
        self.emotionDescription.text = emotionDescriptionLabel
        self.imageEmotion.image = emotionImage
    }
    
   
    @IBAction func backToHome(_ sender: Any) {
        let mainView = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
        self.navigationController?.pushViewController(mainView, animated: true)
    }
    
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
