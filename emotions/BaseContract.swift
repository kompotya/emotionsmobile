//
//  BaseContract.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
protocol BaseContractPresenter {
        func onAttach()
        func onDettach()
    }
protocol BaseContractView : class {
        func initViews ()
        func makeRoundButtons()
        func startLoader()
        func stopLoader()
}
