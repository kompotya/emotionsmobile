//
//  MainViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit

class MainViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {


    let menuItemNames : [String] = ["Теория","Статистика","Практика","Настройки"]
    let menuItemImages : [UIImage] = [UIImage(named : "theory")!, UIImage(named: "statistics")!, UIImage(named: "masks")!, UIImage(named : "settings")!  ]
    @IBOutlet weak var collectionView: UICollectionView!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.collectionView.delegate = self
        self.collectionView.dataSource = self
      
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
       return 1
    }
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuItemNames.count
    }
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell (withReuseIdentifier: "collection_cell_menu_item", for: indexPath as IndexPath) as! MenuItemCollectionViewCell
        cell.menuItemImage.image = menuItemImages[indexPath.row]
        cell.menuItemLabel.text  = menuItemNames[indexPath.row]
        
        return cell
    }

    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        switch indexPath.row {
        case 0:
            let theoryVC = self.storyboard!.instantiateViewController(withIdentifier: "TheoryListViewController") as! TheoryListViewController
            self.navigationController?.pushViewController(theoryVC, animated: true)
            break
        case 1:
            let statisticVC = self.storyboard!.instantiateViewController(withIdentifier: "StatisticViewController") as! StatisticViewController
            self.navigationController?.pushViewController(statisticVC, animated: true)
            break
        case 2:
            let practiceVC = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(practiceVC, animated: true)
            break
        case 3:
            let settingsVC = self.storyboard!.instantiateViewController(withIdentifier: "SettingsViewController") as! SettingsViewController
            self.navigationController?.pushViewController(settingsVC, animated: true)
            break
        default:
            break
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, shouldHighlightItemAt indexPath: IndexPath) -> Bool {
        let cell: MenuItemCollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath) as! MenuItemCollectionViewCell
        cell.menuItemImage.alpha = 0.5
        return true

    }
    
    func collectionView(_ collectionView: UICollectionView, didUnhighlightItemAt indexPath: IndexPath) {
        let cell: MenuItemCollectionViewCell = collectionView.cellForItem(at: indexPath as IndexPath) as! MenuItemCollectionViewCell
        cell.menuItemImage.alpha = 1.0

    }
    
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
        let cellCount = CGFloat(menuItemNames.count)
        if cellCount > 0 {
            let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
            let cellWidth = flowLayout.itemSize.width + flowLayout.minimumInteritemSpacing
            
            let totalCellWidth = cellWidth*cellCount + 10.00 * (cellCount-1)
            let contentWidth = collectionView.frame.size.width - collectionView.contentInset.left - collectionView.contentInset.right
            
            if (totalCellWidth < contentWidth) {
                let padding = (contentWidth - totalCellWidth) / 2.0
                return UIEdgeInsetsMake(0, padding, 0, padding)
            } else {
                if UIInterfaceOrientationIsLandscape(UIApplication.shared.statusBarOrientation) {
                    return UIEdgeInsetsMake(0, 60, 0, 60)

                } else {
                    return UIEdgeInsetsMake(100, 0, 100, 0)

                }
            }
        }
        
        return UIEdgeInsets.zero
    }
    
    override func viewWillLayoutSubviews() {
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
  
    
    
    
}
