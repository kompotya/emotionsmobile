//
//  LoginContract.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
protocol LoginPresenter : BaseContractPresenter {
    func loginUser ()
    
}

protocol LoginView : BaseContractView {
    func addValidators()     
}
