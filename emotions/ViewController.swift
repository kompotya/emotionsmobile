//
//  ViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import EGFormValidator
class ViewController: ValidatorViewController, LoginView{

    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var registrationButton: UIButton!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var errorLabelEmail: UILabel!
    @IBOutlet weak var errorPasswordLabel: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.makeRoundButtons()
        self.addValidators()
        
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.setNavigationBarHidden(true, animated: animated)
    }
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.navigationController?.setNavigationBarHidden(false, animated: animated)
    }
    
    internal func makeRoundButtons() {
        registrationButton.layer.cornerRadius = Constants.CORNER_RADIUS
        loginButton.layer.cornerRadius = Constants.CORNER_RADIUS
    }
    internal func startLoader() {
         print("TODO: SHOW LOADER")
    }
    internal func stopLoader() {
         print("TODO: stop LOADER")
    }
    internal func initViews() {
        print("TODO: init")

    }
    internal func addValidators() {
        self.addValidatorMandatory(toControl: passwordTextField, errorPlaceholder: errorPasswordLabel, errorMessage: "Обязательное поле")
        self.addValidatorMandatory(toControl: emailTextField, errorPlaceholder: errorLabelEmail, errorMessage: "Обязательное поле")
        
        self.addValidatorEmail(toControl: emailTextField, errorPlaceholder: errorLabelEmail, errorMessage: "Неправильный email")
        
        self.addValidatorMinLength(toControl: passwordTextField, errorPlaceholder: errorPasswordLabel, errorMessage: "Минимальная длина - 6", minLength: 6)
    }
    
    @IBAction func loginAction(_ sender: Any) {
        if self.validate() {
            print("valid")
                } else {
            print("not-valid")
            let mainView = self.storyboard!.instantiateViewController(withIdentifier: "MainViewController") as! MainViewController
            self.navigationController?.pushViewController(mainView, animated: true)


    }
    }
}

