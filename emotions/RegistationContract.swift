//
//  RegistationContract.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import Foundation
protocol RegistrationPresenter : BaseContractPresenter {
    func registrateUser()
}
protocol RegistrationView : BaseContractView {
    func addValidators ()
}
