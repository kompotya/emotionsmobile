//
//  RegistrationViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 12.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import EGFormValidator

class RegistrationViewController: ValidatorViewController, RegistrationView {
    
    
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var confirmPasswordTextField: UITextField!
    
    @IBOutlet weak var errorEmailLabel: UILabel!
    @IBOutlet weak var errorPasswordLabel: UILabel!
    @IBOutlet weak var errorConfirmPasswordLabel: UILabel!
    
    @IBOutlet weak var registrationButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.makeRoundButtons()
        self.addValidators()
        
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    

    
    // Internal functions
    internal func startLoader() {
         print("TODO: SHOW LOADER")
    }
    func stopLoader() {
         print("TODO: STOP LOADER")
    }
    
    internal func initViews() {
        print("TODO: Init views")
    }
    internal func makeRoundButtons() {
        self.registrationButton.layer.cornerRadius = Constants.CORNER_RADIUS
    }
    
    internal func addValidators() {
        self.addValidatorMandatory(toControl: emailTextField, errorPlaceholder: errorEmailLabel, errorMessage: "Обязательное поле")
        self.addValidatorMandatory(toControl: passwordTextField, errorPlaceholder: errorPasswordLabel, errorMessage: "Обязательное поле")
        
        self.addValidatorEmail(toControl: emailTextField, errorPlaceholder: errorEmailLabel, errorMessage: "Неправильный email")
        self.addValidatorMinLength(toControl: passwordTextField, errorPlaceholder: errorPasswordLabel, errorMessage: "Минимальная длина - 6", minLength: 6)
        self.addValidatorEqualTo(toControl: confirmPasswordTextField, errorPlaceholder: errorConfirmPasswordLabel, errorMessage: "Пароли не совпадают", compareWithControl: passwordTextField)
        
    }
    
    @IBAction func registrateAction(_ sender: Any) {
                if self.validate() {
                    print("valid")
                } else {
                    print("not-valid")
                }
    }

   
    
 

   

}
