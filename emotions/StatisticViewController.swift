//
//  StatisticViewController.swift
//  emotions
//
//  Created by Michael Ovsienko on 13.04.17.
//  Copyright © 2017 Michael Ovsienko. All rights reserved.
//

import UIKit
import Charts
class StatisticViewController: UIViewController {

    @IBOutlet weak var barChart: BarChartView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        barChart.noDataText = "You need to provide data for the chart."
        barChart.backgroundColor = .clear
        // Do any additional setup after loading the view.
        let months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"]
        let unitsSold = [20.0, 4.0, 6.0, 3.0, 12.0, 16.0, 4.0, 18.0, 2.0, 4.0, 5.0, 4.0]
        
        setChart(dataPoints: months, values: unitsSold)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setChart (dataPoints: [String], values: [Double]){
        let ys1 = Array(1..<3).map { x in return sin(Double(x) / 2.0 / 3.141 * 1.5) }
        let ys2 = Array(1..<10).map { x in return cos(Double(x) / 2.0 / 3.141) }
        let ys3 = Array(1..<10).map { x in return cos(Double(x) / 2.0 / 3.141) * 2}
        let ys4 = Array(1..<10).map { x in return cos(Double(x) / 2.0 / 3.141) * 3}

        let yse1 = ys1.enumerated().map { x, y in return BarChartDataEntry(x: Double(x), y: y) }
        let yse2 = ys2.enumerated().map { x, y in return BarChartDataEntry(x: Double(x), y: y) }
        let yse3 = ys3.enumerated().map { x, y in return BarChartDataEntry(x: Double(x), y: y) }
        let yse4 = ys4.enumerated().map { x, y in return BarChartDataEntry(x: Double(x), y: y) }
       
        
        let data = BarChartData()
        let ds1 = BarChartDataSet(values: yse1, label: "Гнев")
        let ds2 = BarChartDataSet(values: yse2, label: "Презрение")
        let ds3 = BarChartDataSet(values: yse3, label: "Счастье")
        let ds4 = BarChartDataSet(values: yse4, label: "Здоровье")

        ds1.colors = [UIColor.red]
        ds2.colors = [UIColor.yellow]

        ds3.colors = [UIColor.green]

        ds4.colors = [UIColor.blue]

        data.addDataSet(ds1)
        data.addDataSet(ds2)
        data.addDataSet(ds3)
        data.addDataSet(ds4)

        
        self.barChart.data = data
        self.barChart.gridBackgroundColor = UIColor.white
        self.barChart.chartDescription?.text = "Статистика правильных ответов"


    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.barChart.animate(xAxisDuration: 1.0, yAxisDuration: 1.0)
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
